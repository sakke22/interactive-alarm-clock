
from imu import MPU6050
from time import sleep
from machine import Pin, I2C, Timer,PWM
from tm1637 import TM1637
import time

i2c = I2C(0, sda=Pin(0), scl=Pin(1), freq=400000)
imu = MPU6050(i2c)
# Define pins for motor control
enablePin = Pin(18, Pin.OUT)  
motorPin1 = Pin(16, Pin.OUT)  
motorPin2 = Pin(17, Pin.OUT)

display = TM1637(dio = Pin(27), clk = Pin(26))

buzzer_pin = PWM(Pin(15))
buzzer_pin.freq(2000)

# Function to set up the motor control pins
def setup():
    # Initially, turn off the motor
    enablePin.low()
    motorPin1.low()
    motorPin2.low()

# Function to control the motor in one direction
def loop():
    # Turn on the motor in one direction
    enablePin.high()  # Enable the motor
    motorPin1.high()  
    motorPin2.low()   
    
def stop():
    enablePin.low()
    motorPin1.low()
    motorPin2.low()

    
    
    
def countdown():
    for i in range(3, -1, -1):  
        display.numbers(0, i) 
        sleep(1)
    display.brightness(0)
    
def drive_buzzer(duration):
    buzzer_pin.duty_u16(32768)  # 50% duty cycle, modify as needed
    time.sleep(duration)
    buzzer_pin.duty_u16(0)
    time.sleep(duration)


# Setup the motor
setup()
countdown()
while True:
    loop()
    drive_buzzer(0.1)
    ax=round(imu.accel.x,2)
    ay=round(imu.accel.y,2)
    az=round(imu.accel.z,2)
    gx=round(imu.gyro.x)
    gy=round(imu.gyro.y)
    gz=round(imu.gyro.z)
    tem=round(imu.temperature,2)
    print("ax",ax,"\t","ay",ay,"\t","az",az,"\t","gx",gx,"\t","gy",gy,"\t","gz",gz,"\t","Temperature",tem,"        ",end="\r")
    if az<-0.8:
        stop()
        break
    
    

